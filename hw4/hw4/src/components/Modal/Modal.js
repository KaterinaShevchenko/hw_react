import React from "react";
import styles from './Modal.module.scss';
import PropTypes from 'prop-types';

const Modal = (props) => {
    const { closeButton, text, close, actions } = props;

    return(
        <>
        <div className={styles.bg} onClick={close}/>
        <div className={styles.modal}>
                <div className={styles.modal_header}>
                    {closeButton && (
                        <div className={styles.modal_close} onClick={close}>
                            &times;
                        </div>
                    )}
                </div>
                <div className={styles.modal_body}>
                    <p>
                        {text}
                    </p>
                    <div className={styles.modal_footer}>
                        {actions}
                    </div>
                </div>
            </div>
        </>
    )
}

Modal.propTypes = {
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    close: PropTypes.func,
    actions: PropTypes.array
}
Modal.defaultProps = {
    closeButton: false,
    text: '',
    close: () => {},
    actions: []
}

export default Modal;