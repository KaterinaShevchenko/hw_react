import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import styles from './Card.module.scss'
import Button from '../Button';
import Modal from '../Modal';
import PropTypes from 'prop-types';
import { ReactComponent as Fav } from "./img/fav.svg";
import { ReactComponent as RemoveFav } from "./img/removeFav.svg";
import { ReactComponent as Delete } from "./img/delete.svg";
import { useDispatch, useSelector } from 'react-redux';
import { clickModall } from '../../store/actionCreators/modalActionCreator';
import { checkLocation } from '../../store/actionCreators/locationCreators';
import { checkInCart, checkInFav } from '../../store/actionCreators/cartNumberActionCreators';

const Card = (props) => {
    const dispatch = useDispatch();
    const modal = useSelector(({modal}) => modal.modal);
    const inCart = useSelector(({location}) => location.locationCart)
    const [fav, setFav] = useState(false);
    const { allInfo: {name, path, price, article}, checkArticle} = props;

    let location = useLocation();

    useEffect(() => {
        const favorite = JSON.parse(localStorage.getItem('fav'));
        const cart = JSON.parse(localStorage.getItem('cart'));
        if (favorite) {
            dispatch(checkInFav(favorite.length));
            favorite.forEach(item => {
                if (item === article) {
                    setFav(true);
                }
            })
        }
        if (cart) {
            dispatch(checkInCart(cart.length))
        }
        dispatch(checkLocation(location.pathname))
    }, []);


    const clickModal = () => {
        dispatch(clickModall())
    }

    const clickFav = (article) => {
        if (localStorage.getItem('fav')) {
            const fav = JSON.parse(localStorage.getItem('fav'));
            if (!fav.includes(article)) {
                fav.push(article);
                localStorage.setItem('fav', JSON.stringify(fav)); 
                setFav(true)
                dispatch(checkInFav(fav.length));
            } else {
                const newFav = fav.map(item => {
                    if (item !== article) {
                        return item; 
                    }
                })
                const filter = newFav.filter(Number);
                dispatch(checkInFav(filter.length));
                localStorage.setItem('fav', JSON.stringify(filter));
                setFav(false)
            }
        } else {
            localStorage.setItem('fav', JSON.stringify([article]))
            setFav(true)
            dispatch(checkInFav(1));
        }   
    }

    const checkFavIcon = () => {
        return fav ? <RemoveFav onClick={() => clickFav(article)}/> : <Fav onClick={() => clickFav(article)}/>;        
    }

    const checkClass = () => {
        return inCart ? styles.card_cart : styles.card
    }


    return (
        <>
            <div className={checkClass()} id={article}>
                <img src={path} alt={name}/>
                <span className={styles.card_fav}>
                    {inCart && <Delete onClick={() => {clickModal(); checkArticle(article)}}/> }
                    {!inCart && checkFavIcon()}
                </span>
                <h3>{name}</h3>
                <p>Price: {price}</p>
                {!inCart && <Button
                    text='Add to cart'
                    onClick={() => {clickModal(); checkArticle(article)}}
                /> }
            </div>

        </>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.string
}
Card.defaultProps = {
    name: '',
    path: '',
    article: 0,
    price: ''
}

export default Card;