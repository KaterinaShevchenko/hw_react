import React from "react";
import styles from './Header.module.scss'
import PropTypes from 'prop-types';
import { NavLink } from "react-router-dom";
import { ReactComponent as Logo } from "../../assets/svg/logo.svg";
import { ReactComponent as Basket } from "../../assets/svg/basket.svg";
import { ReactComponent as Fav } from "../../assets/svg/fav.svg";
import { useSelector } from "react-redux";


const Header = () => {
    const favNumber = useSelector(({checkItems}) => checkItems.counterFav);
    const cartNumber = useSelector(({checkItems}) => checkItems.counterCart);


    return(
        <header className={styles.header}>
            <NavLink to='/'><Logo/></NavLink>
            <div className={styles.header_block}>
                <NavLink to='/cart'><Basket/></NavLink>
                {cartNumber}
                <NavLink to='/fav'><Fav/></NavLink>
                {favNumber}
            </div>
        </header>
    )
}

export default Header;