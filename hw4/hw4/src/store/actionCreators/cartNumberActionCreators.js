import { CHECK_ITEMS_IN_CART, CHECK_ITEMS_IN_FAV } from "../actions/cartNumberAction";

export const checkInCart = (value) => ({type: CHECK_ITEMS_IN_CART, payload: value})
export const checkInFav = (value) => ({type: CHECK_ITEMS_IN_FAV, payload: value})