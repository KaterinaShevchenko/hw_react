import { LOCATION } from "../actions/locationAction";


export const checkLocation = (value) => ({type: LOCATION, payload: value})