import { GET_ITEMS } from "../actions/itemsAction";

export const getItems = () => async (dispatch) => {
    const items = await fetch ('./items.json').then(res => res.json());
    dispatch({ type: GET_ITEMS, payload: items})
} 