import React from 'react';
import Header from './components/Header';
import Rout from './Routes';
import Title from './components/Title';
import styles from './App.module.scss';
import { Provider } from 'react-redux';
import store from './store';

const App = () => {
    return (
      <>
        <Provider store={store}>
        <Header/>
        <Title/>
        <Rout/>
        </Provider>
      </>
    )
}

export default App;
