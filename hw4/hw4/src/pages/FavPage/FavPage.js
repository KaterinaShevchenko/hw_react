import React, { useEffect } from "react";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import { useDispatch, useSelector } from "react-redux";
import { getItems } from "../../store/actionCreators/itemsActionCreators";

const FavPage = () => {
    const dispatch = useDispatch();
    const items = useSelector(({items}) => items.items);
    useSelector(({checkItems}) => checkItems.counterFav);
    
    useEffect(() => {
        dispatch(getItems())
    }, [])

    const findItemsFav = () => {
        const itemsCart = JSON.parse(localStorage.getItem('fav'));
        return items.reduce((arrayCart, item) => {
            if (Array.isArray(itemsCart)) {
                if (itemsCart.includes(item.article)) {
                    arrayCart.push(item)
                }
            }
            return arrayCart
        }, []);

    }


    return(
        <>
            {items && <CardsContainer 
                items={findItemsFav()}                 
            />}
        </>
    )
}

export default FavPage;