import React from 'react';
import { Route, Routes } from 'react-router';
import CartPage from '../pages/CartPage/CartPage';
import FavPage from '../pages/FavPage/FavPage';
import HomePage from '../pages/HomePage/HomePage'

const Rout = () => {
    return (
        <Routes>
            <Route exact='true' path='/' element={<HomePage/>}/>
            <Route exact='true' path='/cart' element={<CartPage/>}/>
            <Route exact='true' path='/fav' element={<FavPage/>}/>
      </Routes>
    )
}

export default Rout;