import React, { useEffect, useState } from "react";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import { useDispatch, useSelector } from "react-redux";
import { getItems } from "../../store/actionCreators/itemsActionCreators";
import FormInCart from "../../components/Formik";
import Button from "../../components/Button";
import Modal from "../../components/Modal";
import { clickModall } from "../../store/actionCreators/modalActionCreator";
import { checkInCart } from '../../store/actionCreators/cartNumberActionCreators';
import styles from './CartPage.module.scss';

const CartPage = () => {
    const dispatch = useDispatch();
    const items = useSelector(({items}) => items.items)
    const modal = useSelector(({modal}) => modal.modal);
    const inCart = useSelector(({location}) => location.locationCart)
    const [article, setArticle] = useState();
    const checkItems = useSelector(({checkItems}) => checkItems.counterCart);
    const [info, setInfo] = useState()
    

    useEffect(() => {
        dispatch(getItems())
    }, [])

    const clickModal = () => {
        dispatch(clickModall())
    }

    const checkArticle = (value) => {
        setArticle(value);
    }

    const findItemsCart = () => {
        const itemsCart = JSON.parse(localStorage.getItem('cart'));
        return items.reduce((arrayCart, item) => {
            if (Array.isArray(itemsCart)) {
                if (itemsCart.includes(item.article)) {
                    arrayCart.push(item)
                }
            }
            return arrayCart
        }, []);
    }

    const removeFromCart = (article) => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            const newCart = cart.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newCart.filter(Number);
            localStorage.setItem('cart', JSON.stringify(filter));
            dispatch(checkInCart(filter.length))
        }
        clickModal();
    }

    const infoAboutItems = () => {

    }


    return(
        <>
            {items && 
            <div className={styles.container}>
                <CardsContainer 
                items={findItemsCart()}  
                checkArticle={checkArticle}
                />
                {checkItems > 0 ? <FormInCart infoAboutItems={findItemsCart()}/> : null }
            </div>
           }
            {inCart &&
            modal && <Modal
                closeButton={true}
                close={() => clickModal()}
                text='Remove from cart?'
                actions={[
                    <Button
                        key={1}
                        text="Yes"
                        onClick={() => removeFromCart(article)}
                    />,
                    <Button
                        key={2}
                        text="No"
                        onClick={() => clickModal()}
                    />
                ]}
            />}
        </>
    )
}

export default CartPage;