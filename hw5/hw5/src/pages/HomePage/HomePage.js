import React, { useEffect, useState } from "react";
import CardsContainer from "../../components/CardsContainer/CardsContainer";
import { useDispatch, useSelector } from "react-redux";
import { getItems } from "../../store/actionCreators/itemsActionCreators";
import Modal from "../../components/Modal";
import Button from "../../components/Button";
import { clickModall } from "../../store/actionCreators/modalActionCreator";
import { checkInCart } from '../../store/actionCreators/cartNumberActionCreators';

const HomePage = () => {
    const dispatch = useDispatch();
    const items = useSelector(({items}) => items.items)
    const modal = useSelector(({modal}) => modal.modal);
    const inCart = useSelector(({location}) => location.locationCart)
    const [article, setArticle] = useState();


    useEffect(() => {
        dispatch(getItems())
    }, []);

    const clickModal = () => {
        dispatch(clickModall())
    }

    const checkArticle = (value) => {
        setArticle(value);
    }

    const addToCart = (article) => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            if (!cart.includes(article)) {
                cart.push(article);
                localStorage.setItem('cart', JSON.stringify(cart))
                dispatch(checkInCart(cart.length))
            }
        } else {
            localStorage.setItem('cart', JSON.stringify([article]))
            dispatch(checkInCart(1))
        }
        clickModal();
    }

    return (
        <>
        {items && <CardsContainer  
            items={items} 
            checkArticle={checkArticle}
            /> }
        {!inCart &&
        modal && <Modal
            closeButton={true}
            close={() => clickModal()}
            text='Add to cart?'
            actions={[
                <Button
                    key={1}
                    text="Yes"
                    onClick={() => addToCart(article)}
                />,
                <Button
                    key={2}
                    text="No"
                    onClick={() => clickModal()}
                />
            ]}
        />}
        </>
        
    )
}

export default HomePage;