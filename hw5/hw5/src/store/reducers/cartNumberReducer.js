import { CHECK_ITEMS_IN_CART, CHECK_ITEMS_IN_FAV } from "../actions/cartNumberAction";

const initialState = {
    counterCart: 0,
    counterFav: 0
}


const cartNumberReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHECK_ITEMS_IN_CART: {
            return {...state, counterCart: action.payload}
        }
        case CHECK_ITEMS_IN_FAV: {
            return {...state, counterFav: action.payload}
        }
        default: {
            return state;
        }
    }
}

export default cartNumberReducer;