import { CLICK_MODAL } from "../actions/modalAction";

const initialState = {
    modal: false
}

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case CLICK_MODAL: {
            return {...state, modal: !state.modal}
        }
        default: {
            return state;
        }
    }
}

export default modalReducer;