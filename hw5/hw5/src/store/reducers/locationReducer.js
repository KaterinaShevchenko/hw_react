import { LOCATION } from "../actions/locationAction";

const initialState = {
    locationCart: false,
    locationFav: false
}

const locationReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOCATION: {
            if (action.payload === '/cart') {
                return {...state, locationCart: true}
            } else if (action.payload === '/fav') {
                return {...state, locationCart: false, locationFav: true}
            } else { 
                return {...state, locationCart: false, locationFav: false}
            }
        }
        default: {
            return state;
        }
    }
}

export default locationReducer;