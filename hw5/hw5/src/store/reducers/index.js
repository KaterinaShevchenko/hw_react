import { combineReducers } from "redux";
import cartNumberReducer from "./cartNumberReducer";
import itemReducer from "./itemReducer";
import locationReducer from "./locationReducer";
import modalReducer from "./modalReducer";


const appReducer = combineReducers({
    items: itemReducer,
    modal: modalReducer,
    location: locationReducer,
    checkItems: cartNumberReducer
})

export default appReducer;