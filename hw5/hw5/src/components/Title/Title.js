import React, { useEffect } from "react";
import { useLocation } from "react-router-dom";
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from "react-redux";
import { checkLocation } from "../../store/actionCreators/locationCreators";

const Title = () => {
    const dispatch = useDispatch();
    const inCart = useSelector(({location}) => location.locationCart)
    const inFav = useSelector(({location}) => location.locationFav)
    const cartNubmer = useSelector(({checkItems}) => checkItems.counterCart);
    const favNumber = useSelector(({checkItems}) => checkItems.counterFav);
    const location = useLocation();

    useEffect(() => {
        dispatch(checkLocation(location.pathname))
    }, [])

    const checkTitle = () => {
        if (inFav) {
            return favNumber > 0 ? 'Your favorited items' : 'No items'
        } else if (inCart) {
            return cartNubmer > 0 ? 'Your added items to the cart' : 'No items in cart'
        } else {
            return 'All items'
        }
    }

    return (
        <>
         <h1>{checkTitle()}</h1>
        </>
    )
}

export default Title;