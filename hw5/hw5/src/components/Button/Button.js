import React from "react";
import PropTypes from 'prop-types';
import styles from './Button.module.scss'

const Button = (props) => {
    const { text, onClick, className, type} = props;
    return(
        <button 
            className = {className}
            onClick = {onClick}
            type= {type}
        >
            {text}
        </button>
    )
}

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    type: PropTypes.oneOf(['submit', 'button'])
}
Button.defaultProps = {
    text: '',
    type: 'button',
    className: '',
    onClick: () => {}
}

export default Button;