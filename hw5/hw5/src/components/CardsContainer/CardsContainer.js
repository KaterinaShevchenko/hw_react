import React, { useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { useLocation } from "react-router-dom";
import Card from "../Card";
import styles from './CardsContainer.module.scss'
import { useDispatch, useSelector } from "react-redux";
import { checkLocation } from '../../store/actionCreators/locationCreators';





const CardsContainer = (props) => {
    const dispatch = useDispatch();
    const inCart = useSelector(({location}) => location.location)
    const {items, checkArticle} = props;
   

    const location = useLocation();

    useEffect(() => {
        dispatch(checkLocation(location.pathname))
    }, [])

    const checkClass = () => {
        return inCart ? styles.cards_container_cart : styles.cards_container;
    }

    return(
        <div className={checkClass()}>
            {items && items.map((item) => 
                <Card 
                    key={item.article} 
                    allInfo={item} 
                    checkArticle={checkArticle}
                />)}
        </div>
    ) 
}

CardsContainer.propTypes = {
    items: PropTypes.array
}
CardsContainer.defaultProps ={
    items: []
}

export default CardsContainer;