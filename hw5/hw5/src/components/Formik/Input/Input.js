import React from "react";
import styles from './Input.module.scss';

const Input = (props) => {
    const {type, placeholder, lable, id, form, field} = props;
    // console.log(form);
    return (
        <div className={styles.block}>
            <label htmlFor="">{lable}</label>
            <input
                value={field.value}
                onChange={field.onChange}
                onBlur={field.onBlur}
                id={id}
                type={type}
                placeholder={placeholder}
            />
        </div>
    )
}

export default Input;