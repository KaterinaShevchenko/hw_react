import React from "react";
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as yup from 'yup'; 
import Input from "./Input";
import styles from './Form.module.scss';
import { useDispatch } from "react-redux";
import { checkInCart } from "../../store/actionCreators/cartNumberActionCreators";





const initialValues = {
    name: '',
    lastname: '',
    age: 0,
    adress: '',
    mobile: 0

}

const validationSchema = yup.object().shape({
    name: yup
            .string()
            .required("Name is required")
            .min(3, 'Name shuld be largest')
            .max(15, 'Name is so large'),
    lastname: yup
                .string()
                .required('Lastname is required'),
    age: yup
            .number()
            .required('Age is required')
            .min(18, 'You must be 18 y.o.')
            .max(100, 'Enter your real age'),
    adress: yup
            .string()
            .required('Adress is required'),
    mobile: yup
            .number()
            .required('Phone number is required')
})

const FormInCart = (props) => {
    const dispatch = useDispatch();
    const {infoAboutItems} = props;

    const onSubmit = (value) => {
        localStorage.removeItem('cart')
        dispatch(checkInCart(0))
        console.log({infoAboutPerson: value, infoAboutItems: infoAboutItems});
    }
    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {({values}) => {
                return (<Form>
                    <div className={styles.block}>
                        <Field
                            type="text"
                            name="name"
                            placeholder="Your name"
                            id='name'
                            lable="Your name"
                            component={Input}
                        />
                        <span className={styles.error}><ErrorMessage name="name"/></span>
                    </div>
                    <div className={styles.block}>
                        <Field
                            type="text"
                            name="lastname"
                            placeholder="Your lastname"
                            id='lastname'
                            lable="Your lastname"
                            component={Input}
                        />
                        <span className={styles.error}><ErrorMessage name="lastname"/></span>
                    </div>
                    <div className={styles.block}>
                        <Field
                            type="text"
                            name="age"
                            placeholder="Your age"
                            id='age'
                            lable="Your age"
                            component={Input}
                        />
                        <span className={styles.error}><ErrorMessage name="age"/></span>
                    </div>
                    <div className={styles.block}>
                        <Field
                            type="adress"
                            name="adress"
                            placeholder="Your adress"
                            id='adress'
                            lable="Your adress"
                            component={Input}
                        />
                        <span className={styles.error}><ErrorMessage name="adress"/></span>
                    </div>
                    <div className={styles.block}>
                        <Field
                            type="mobile"
                            name="mobile"
                            placeholder="Your mobile"
                            id='mobile'
                            lable="Your mobile"
                            component={Input}
                        />
                        <span className={styles.error}><ErrorMessage name="mobile"/></span>
                    </div>

                    <button disabled={!values.name} type="submit">Checkout</button>
                </Form>)
            }}
        
        </Formik>
    )
}

export default FormInCart;