import React from "react";
import { useLocation } from "react-router-dom";
import PropTypes from 'prop-types';

const Title = (props) => {
    const { cartNubmer, favNumber} = props;
    let location = useLocation();

    const checkTitle = () => {
        if (location.pathname === '/fav') {
            return favNumber > 0 ? 'Your favorited items' : 'No items'
        } else if (location.pathname === '/cart') {
            return cartNubmer > 0 ? 'Your added items to the cart' : 'No items in cart'
        } else {
            return 'All items'
        }
    }

    return (
        <>
         <h1>{checkTitle()}</h1>
        </>
    )
}

Title.propTypes = {
    cartNubmer: PropTypes.number,
    favNumber: PropTypes.number
}
Title.defaultProps = {
    cartNubmer: 0,
    favNumber: 0
}

export default Title;