import React, { useEffect, useState } from "react";
import PropTypes from 'prop-types';
import { useLocation } from "react-router-dom";
import Card from "../Card";
import styles from './CardsContainer.module.scss'




const CardsContainer = (props) => {
    const [inCart, setInCart] = useState();
    const {items, updateCartNumber, updateFavNumber} = props;

    let location = useLocation();

    useEffect(() => {
        location.pathname === '/cart' ? setInCart(true) :  setInCart(false);
    }, [])

    const checkClass = () => {
        return inCart ? styles.cards_container_cart : styles.cards_container;
    }

    return(
        <div className={checkClass()}>
            {items && items.map((item) => 
                <Card 
                    key={item.article} 
                    allInfo={item} 
                    updateCartNumber={updateCartNumber} 
                    updateFavNumber={updateFavNumber}
                />)}
        </div>
    ) 
}

CardsContainer.propTypes = {
    updateCartNumber: PropTypes.func, 
    updateFavNumber: PropTypes.func
}
CardsContainer.defaultProps ={
    updateCartNumber: () => {}, 
    updateFavNumber: () => {} 
}

export default CardsContainer;