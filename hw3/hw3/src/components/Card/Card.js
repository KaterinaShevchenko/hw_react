import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import styles from './Card.module.scss'
import Button from '../Button';
import Modal from '../Modal';
import PropTypes from 'prop-types';
import { ReactComponent as Fav } from "./img/fav.svg";
import { ReactComponent as RemoveFav } from "./img/removeFav.svg";
import { ReactComponent as Delete } from "./img/delete.svg";

const Card = (props) => {
    const [modal, setModal] = useState(false);
    const [fav, setFav] = useState(false);
    const [inCart, setInCart] = useState();
    const { allInfo: {name, path, price, article}, updateCartNumber, updateFavNumber} = props;

    let location = useLocation();

    useEffect(() => {
        const favorite = JSON.parse(localStorage.getItem('fav'));
        const cart = JSON.parse(localStorage.getItem('cart'));
        if (favorite) {
            updateFavNumber(favorite.length)
            favorite.forEach(item => {
                if (item === article) {
                    setFav(true);
                }
            })
        }
        if (cart) {
            updateCartNumber(cart.length)
        }
        
        location.pathname === '/cart' ? setInCart(true) :  setInCart(false);
    }, []);


    const clickModal = () => {
        setModal(!modal);
    }

    const clickFav = (article) => {
        if (localStorage.getItem('fav')) {
            const fav = JSON.parse(localStorage.getItem('fav'));
            if (!fav.includes(article)) {
                fav.push(article);
                localStorage.setItem('fav', JSON.stringify(fav)); 
                setFav(true)
                updateFavNumber(fav.length)
            } else {
                const newFav = fav.map(item => {
                    if (item !== article) {
                        return item; 
                    }
                })
                const filter = newFav.filter(Number);
                updateFavNumber(filter.length)
                localStorage.setItem('fav', JSON.stringify(filter));
                setFav(false)
            }
        } else {
            localStorage.setItem('fav', JSON.stringify([article]))
            setFav(true)
            updateFavNumber(1)
        }   
    }

    const addToCart = (article) => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            if (!cart.includes(article)) {
                cart.push(article);
                localStorage.setItem('cart', JSON.stringify(cart))
                updateCartNumber(cart.length)  
            }
            
        } else {
            localStorage.setItem('cart', JSON.stringify([article]))
            updateCartNumber(1)  
        }
        clickModal();
    }

    const removeFromCart = (article) => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            const newCart = cart.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newCart.filter(Number);
            localStorage.setItem('cart', JSON.stringify(filter));
            updateCartNumber(filter.length)
        }
        clickModal();
    }

    const checkFavIcon = () => {
        return fav ? <RemoveFav onClick={() => clickFav(article)}/> : <Fav onClick={() => clickFav(article)}/>;        
    }

    const checkClass = () => {
        return inCart ? styles.card_cart : styles.card
    }


    return (
        <>
            <div className={checkClass()} id={article}>
                <img src={path} alt={name}/>
                <span className={styles.card_fav}>
                    {inCart && <Delete onClick={() => clickModal()}/> }
                    {!inCart && checkFavIcon()}
                </span>
                <h3>{name}</h3>
                <p>Price: {price}</p>
                {!inCart && <Button
                    text='Add to cart'
                    onClick={() => clickModal()}
                /> }
            </div>
            {!inCart &&
            modal && <Modal
                closeButton={true}
                close={() => clickModal()}
                text='Add to cart?'
                actions={[
                    <Button
                        key={1}
                        text="Yes"
                        onClick={() => addToCart(article)}
                    />,
                    <Button
                        key={2}
                        text="No"
                        onClick={() => clickModal()}
                    />
                ]}
            />}
            {inCart &&
            modal && <Modal
                closeButton={true}
                close={() => clickModal()}
                text='Remove from cart?'
                actions={[
                    <Button
                        key={1}
                        text="Yes"
                        onClick={() => removeFromCart(article)}
                    />,
                    <Button
                        key={2}
                        text="No"
                        onClick={() => clickModal()}
                    />
                ]}
            />}
        </>
    )
}

Card.propTypes = {
    name: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.string,
    updateCartNumber: PropTypes.func, 
    updateFavNumber: PropTypes.func
}
Card.defaultProps = {
    name: '',
    path: '',
    article: 0,
    price: '',
    updateCartNumber: () => {}, 
    updateFavNumber: () => {} 
}

export default Card;