import React from 'react';
import { Route, Routes } from 'react-router';
import HomePage from '../pages/HomePage';
import CartPage from '../pages/CartPage/CartPage';
import FavPage from '../pages/FavPage/FavPage';
import PropTypes from 'prop-types';

const Rout = (props) => {
    const {updateCartNumber, updateFavNumber} = props;
    
    return (
        <Routes>
            <Route exact='true' path='/' element={<HomePage updateCartNumber={updateCartNumber} updateFavNumber={updateFavNumber}/>}/>
            <Route exact='true' path='/cart' element={<CartPage updateCartNumber={updateCartNumber} updateFavNumber={updateFavNumber}/>}/>
            <Route exact='true' path='/fav' element={<FavPage updateCartNumber={updateCartNumber} updateFavNumber={updateFavNumber}/>}/>
      </Routes>
    )
}

Rout.propTypes = {
    updateCartNumber: PropTypes.func, 
    updateFavNumber: PropTypes.func
}
Rout.defaultProps ={
    updateCartNumber: () => {}, 
    updateFavNumber: () => {} 
}

export default Rout;