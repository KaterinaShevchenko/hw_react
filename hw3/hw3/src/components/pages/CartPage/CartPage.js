import React, { useEffect, useState } from "react";
import CardsContainer from "../../CardsContainer";
import PropTypes from 'prop-types';

const CartPage = (props) => {
    const [items, setItems] = useState();
    const {updateCartNumber, updateFavNumber} = props;

    useEffect(() => {
        (async () => {
            const items = await fetch ('./items.json').then(res => res.json());
            setItems(items)
        } )()
    }, [])

    const findItemsCart = () => {
        const itemsCart = JSON.parse(localStorage.getItem('cart'));
        return items.reduce((arrayCart, item) => {
            if (Array.isArray(itemsCart)) {
                if (itemsCart.includes(item.article)) {
                    arrayCart.push(item)
                }
            }
            return arrayCart
        }, []);
    }


    return(
        <>
            {items && <CardsContainer 
                items={findItemsCart()} 
                updateCartNumber={updateCartNumber} 
                updateFavNumber={updateFavNumber}
            />}
        </>
    )
}

CartPage.propTypes = {
    updateCartNumber: PropTypes.func, 
    updateFavNumber: PropTypes.func
}
CartPage.defaultProps ={
    updateCartNumber: () => {}, 
    updateFavNumber: () => {} 
}

export default CartPage;