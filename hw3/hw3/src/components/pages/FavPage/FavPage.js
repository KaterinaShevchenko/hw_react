import React, { useEffect, useState } from "react";
import CardsContainer from "../../CardsContainer";
import PropTypes from 'prop-types';

const FavPage = (props) => {
    const [items, setItems] = useState();
    const {updateCartNumber, updateFavNumber} = props;

    
    useEffect(() => {
        (async () => {
            const items = await fetch ('./items.json').then(res => res.json());
            setItems(items)
        } )()

    }, [])

    const findItemsFav = () => {
        const itemsCart = JSON.parse(localStorage.getItem('fav'));
        return items.reduce((arrayCart, item) => {
            if (Array.isArray(itemsCart)) {
                if (itemsCart.includes(item.article)) {
                    arrayCart.push(item)
                }
            }
            return arrayCart
        }, []);

    }


    return(
        <>
            {items && <CardsContainer 
                items={findItemsFav()} 
                updateCartNumber={updateCartNumber} 
                updateFavNumber={updateFavNumber}
            />}
        </>
    )
}

FavPage.propTypes = {
    updateCartNumber: PropTypes.func, 
    updateFavNumber: PropTypes.func
}
FavPage.defaultProps ={
    updateCartNumber: () => {}, 
    updateFavNumber: () => {} 
}

export default FavPage;