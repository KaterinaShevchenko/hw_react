import React, { useState, useEffect } from "react";
import CardsContainer from "../../CardsContainer";
import PropTypes from 'prop-types';

const HomePage = (props) => {
    const [items, setItems] = useState(null);
    const {updateCartNumber, updateFavNumber} = props;

    useEffect(() => {
        (async () => {
            const items = await fetch ('./items.json').then(res => res.json());
            setItems(items)
        })()
    }, []);

    return (
        <CardsContainer 
            items={items} 
            updateCartNumber={updateCartNumber} 
            updateFavNumber={updateFavNumber}
        />
    )
}

HomePage.propTypes = {
    updateCartNumber: PropTypes.func, 
    updateFavNumber: PropTypes.func
}
HomePage.defaultProps ={
    updateCartNumber: () => {}, 
    updateFavNumber: () => {} 
}

export default HomePage;