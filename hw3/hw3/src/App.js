import React, { useState } from 'react';
import Header from './components/Header';
import { Route, Routes  } from 'react-router-dom';
import Rout from './components/Routes';
import Title from './components/Title';
import styles from './App.module.scss';

const App = () => {
  const [cartNubmer, setCartNubmer] = useState();
  const [favNumber, setFavNumber] = useState();

  const updateCartNumber = (value) => {
    setCartNubmer(value)
  }

  const updateFavNumber = (value) => {
    setFavNumber(value)
  }

    return (
      <>
        <Header cartNubmer={cartNubmer} favNumber={favNumber}/>
        <Title cartNubmer={cartNubmer} favNumber={favNumber}/>
        <Rout updateCartNumber={updateCartNumber} updateFavNumber={updateFavNumber}/>
      </>
    )
}

export default App;
