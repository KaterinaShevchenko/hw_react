import React, {Component} from "react";
import styles from './Modal.module.scss';

class Modal extends Component {
    render() {
        const { headerText, closeButton, text, close, actions } = this.props;

        return (
            <>
            <div className={styles.bg} onClick={close}/>
            <div className={styles.modal}>
                    <div className={styles.modal_header}>
                        <h2>
                           {headerText} 
                        </h2>
                        {closeButton && (
                            <div className={styles.modal_close} onClick={close}>
                                &times;
                            </div>
                        )}
                    </div>
                    <div className={styles.modal_body}>
                        <p>
                            {text}
                        </p>
                        <div className={styles.modal_footer}>
                            {actions}
                        </div>
                    </div>
                </div>
            </>
     
        )
    }
}

export default Modal;