import React, { Component } from 'react';
import Button from './components/Button';
import Modal from './components/Modal';

class App extends Component {
  state = {
    modalFirst: false,
    modalSecond: false
  }

  openFirstModal = () => {
      this.setState({modalFirst: !this.state.modalFirst})
  }
  openSecondModal = () => {
  this.setState({modalSecond: !this.state.modalSecond})
  }

  render() {
    const { modalFirst, modalSecond } = this.state;
    return (
      <div className="App">
        <Button
          text='Open first modal'
          backgroundColor='yellowgreen'
          onClick={this.openFirstModal}
        />
        <Button
          text='Open second modal'
          backgroundColor='aliceblue'
          onClick={this.openSecondModal}
        />
        {modalFirst && (
          <Modal
          headerText="Do you want to delete this file?"
          closeButton={false}
          text="Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete?"
          close={this.openFirstModal}
          actions={[
            <Button
              text='OK'
              onClick={this.openFirstModal}
            />,
            <Button
              text='Cancel'
              onClick={this.openFirstModal}
            />
          ]}
          />
        )}
        {modalSecond && (
          <Modal
          headerText="Delete this file?"
          closeButton={true}
          text="Are you sure you want to delete?"
          close={this.openSecondModal}
          actions={[
            <Button
              text='OK'
              onClick={this.openSecondModal}
            />,
            <Button
              text='Cancel'
              onClick={this.openSecondModal}
            />
          ]}
          />
        )}
      </div>
    )
  }
}

export default App;
