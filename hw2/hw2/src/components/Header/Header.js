import React, { Component } from "react";
import styles from './Header.module.scss'
import { ReactComponent as Logo } from "../../assets/svg/logo.svg";
import { ReactComponent as Basket } from "../../assets/svg/basket.svg";
import { ReactComponent as Fav } from "../../assets/svg/fav.svg";


class Header extends Component {
    state = {
        itemsInCart: 0,
        itemsInFav: 0
    }

    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const fav = JSON.parse(localStorage.getItem('fav'));
        if (cart) {
            this.setState({itemsInCart: cart.length})
        }
        if (fav) {
            this.setState({itemsInFav: fav.length})
        }
    }

    showNumberCart = () => {
        if (this.state.itemsInCart !== 0) {
            return <span>{this.state.itemsInCart}</span>
        }
    }

    showNumberFav = () => {
        if (this.state.itemsInFav !== 0) {
            return <span>{this.state.itemsInFav}</span>
        }
    }
    render() {
        return(
            <header className={styles.header}>
                {<Logo/>}
                <div className={styles.header_block}>
                    {<Basket/>}
                    {this.showNumberCart()}
                    {<Fav/>}
                    {this.showNumberFav()}
                </div>
            </header>
        )
    }
}

export default Header;