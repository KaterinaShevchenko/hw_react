import React, { Component } from "react";
import Card from "../Card";
import styles from './CardsContainer.module.scss'

class CardsContainer extends Component {
    state = {
        items: []
    }

    async componentDidMount() {
        const items = await fetch ('./items.json').then(res => res.json());
        this.setState({items})
    }

    render() {
        const {items} = this.state;
        return(
            <div className={styles.cards_container}>
            {items.map((item) => <Card key={item.article} allInfo={item}/>)}
            </div>
        )
    }
}

export default CardsContainer;