import React, {Component} from 'react';
import styles from './Card.module.scss'
import Button from '../Button';
import Modal from '../Modal';
import PropTypes from 'prop-types';
import { ReactComponent as Fav } from "../../assets/svg/fav.svg";
import { ReactComponent as RemoveFav } from "../../assets/svg/removeFav.svg";

class Card extends Component {
    state = {
        modal: false,
        cart: [],
        fav: []
    }

    componentDidMount() {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const fav = JSON.parse(localStorage.getItem('fav'));
        if (cart) {
            this.setState({cart: cart})
        }
        if (fav) {
            this.setState({fav: fav})
        }

    }


    clickModal = () => {
        this.setState({modal: !this.state.modal})
    }

    addToFav = (article) => {
        if (localStorage.getItem('fav')) {
            const fav = JSON.parse(localStorage.getItem('fav'));
            if (!fav.includes(article)) {
                fav.push(article);
              localStorage.setItem('fav', JSON.stringify(fav));
              this.setState({fav: fav})
            }
          } else {
            localStorage.setItem('fav', JSON.stringify([article]))
          }
    }

    removeFromFav = (article) => {
        if (localStorage.getItem('cart')) {
            const cart = JSON.parse(localStorage.getItem('cart'));
            const newCart = cart.map(item => {
                if (item !== article) {
                    return item; 
                }
            })
            const filter = newCart.filter(Number);
            localStorage.setItem('fav', JSON.stringify(filter));
            this.setState({fav: filter})
        }
    }

    addToCart = (article) => {
        const cart = JSON.parse(localStorage.getItem('cart'));
        if (cart) {
            if (!cart.includes(article)) {
                cart.push(article);
                localStorage.setItem('cart', JSON.stringify(cart))
            }
        } else {
            localStorage.setItem('cart', JSON.stringify([article]))
        }
        this.clickModal();
    }

    checkFavIcon = (article) => {
        if (this.state.fav.includes(article)) {
            return <RemoveFav onClick={() => this.removeFromFav(article)}/>
        } else {
            return <Fav onClick={() => this.addToFav(article)}/>
        }
    }



    render() {
        const {modal} = this.state;
        const { allInfo: {name, path, price, article}} = this.props;
   
        return (
            <>
            <div className={styles.card}>
            <img src={path} alt={name}/>
            <span className={styles.card_fav}>
                {this.checkFavIcon(article)}
            </span>
            <h3>{name}</h3>
            <p>Price: {price}</p>
            <Button
                text='Add to cart'
                onClick={this.clickModal}
            />
            </div>
            {modal && <Modal
                closeButton={true}
                close={this.clickModal}
                text='Add to cart?'
                actions={[
                    <Button
                        key={1}
                        text="Yes"
                        onClick={() => this.addToCart(article)}
                    />,
                    <Button
                        key={2}
                        text="No"
                        onClick={this.clickModal}
                    />
                ]}
            />}
            </>

        )
    }
}

Card.propTypes = {
    name: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.number,
    price: PropTypes.string
}
Card.defaultProps = {
    name: '',
    path: '',
    article: 0,
    price: ''
}

export default Card;