import React, {Component} from 'react';
// import Card from './components/Card';
import CardsContainer from './components/CardsContainer';
import Header from './components/Header';

class App extends Component {
  render() {
    return (
      <>
        <Header/>
        <CardsContainer/>
      </>
    )
  }
}

export default App;
